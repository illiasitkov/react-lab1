import React from 'react';
import './Textarea.css';

export const Textarea = ({
	id,
	labelText,
	value,
	onChange,
	placeholderText,
}) => {
	return (
		<div>
			<label htmlFor={id}>{labelText}</label>
			<br />
			<textarea
				id={id}
				value={value}
				onChange={onChange}
				minLength={2}
				required={true}
				rows={4}
				placeholder={placeholderText}
			/>
		</div>
	);
};
