import React from 'react';
import './Input.css';

export const Input = ({
	type,
	width = '',
	id,
	labelText,
	inputPlaceholder,
	onChange,
	value,
	min = 0,
}) => {
	return (
		<div>
			<label htmlFor={id}>{labelText}</label>
			{labelText && <br />}
			<input
				style={{ width }}
				required={true}
				id={id}
				type={type}
				value={value}
				onChange={onChange}
				placeholder={inputPlaceholder}
				min={min}
			/>
		</div>
	);
};
