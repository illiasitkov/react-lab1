import React from 'react';
import './CourseCard.css';
import { Button } from '../../../../common/Button/Button';
import { durationPipeToString } from '../../../../helpers/durationPipe';
import { dateConverterPipe } from '../../../../helpers/datePipe';
import { CourseDetail } from './components/CourseDetail/CourseDetail';
import { AUTHORS, CREATED, DURATION, SHOW_COURSE } from '../../../../constants';

export const CourseCard = ({ course, authors }) => {
	const authorsList = course.authors.map(
		(id) => authors.filter((a) => a.id === id)[0].name
	);
	const authorsListString = authorsList.join(', ');
	const duration = durationPipeToString(course.duration);
	const createdDate = dateConverterPipe(course.creationDate);
	return (
		<div className='course-card'>
			<div className='row'>
				<div className='col-8'>
					<h2 className='mb-3'>
						<strong>{course.title}</strong>
					</h2>
					<div>{course.description}</div>
				</div>
				<div className='col-4'>
					<CourseDetail title={AUTHORS} text={authorsListString} />
					<CourseDetail title={DURATION} text={duration + ' hours'} />
					<CourseDetail title={CREATED} text={createdDate} />
					<div className='mt-4 d-flex justify-content-center'>
						<Button buttonText={SHOW_COURSE} />
					</div>
				</div>
			</div>
		</div>
	);
};
