import React from 'react';
import './CourseDetail.css';

export const CourseDetail = ({ title, text }) => {
	return (
		<div className='d-flex mb-2'>
			<strong>{title}:&nbsp;</strong>
			<div className='text-ellipsis'>{text}</div>
		</div>
	);
};
