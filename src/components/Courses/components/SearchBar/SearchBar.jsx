import React, { useState } from 'react';
import { Input } from '../../../../common/Input/Input';
import { Button } from '../../../../common/Button/Button';
import { changeHandler } from '../../../../helpers/changeHandler';
import { BTN_SEARCH, SEARCH_PLACEHOLDER } from '../../../../constants';

export const SearchBar = ({ onSearch }) => {
	const [searchStr, setSearchStr] = useState('');

	return (
		<div className='d-flex gap-3 align-items-center flex-wrap'>
			<Input
				width='350px'
				onChange={changeHandler(setSearchStr)}
				value={searchStr}
				id='searchText'
				type='search'
				labelText=''
				inputPlaceholder={SEARCH_PLACEHOLDER}
			/>
			<Button buttonText={BTN_SEARCH} onClick={() => onSearch(searchStr)} />
		</div>
	);
};
