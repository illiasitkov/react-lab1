import React, { useState } from 'react';
import { CourseCard } from './components/CourseCard/CourseCard';
import { SearchBar } from './components/SearchBar/SearchBar';
import { Button } from '../../common/Button/Button';
import { useNavigate } from 'react-router-dom';
import { BTN_ADD_COURSE } from '../../constants';

export const Courses = ({ courses, authors }) => {
	const navigate = useNavigate();

	const [searchStr, setSearchStr] = useState('');

	const filter = (string) => (course) => {
		let stringLower = string.toLowerCase();
		return (
			course.title.toLowerCase().includes(stringLower) ||
			course.id.toLowerCase().includes(stringLower)
		);
	};

	const navigateToCreateCourse = () => {
		navigate('/courses/new', { replace: true });
	};

	const views = courses.filter(filter(searchStr)).map((course) => {
		return (
			<div key={course.id} className='mb-4'>
				<CourseCard course={course} authors={authors} />
			</div>
		);
	});

	return (
		<section className='content-wrapper'>
			<div className='d-flex justify-content-between align-items-center mb-4 flex-wrap'>
				<SearchBar onSearch={setSearchStr} />
				<Button buttonText={BTN_ADD_COURSE} onClick={navigateToCreateCourse} />
			</div>
			<div>{views}</div>
		</section>
	);
};
