import React from 'react';
import './Header.css';
import { Logo } from './components/Logo/Logo';
import { LOGOUT, USERNAME } from '../../constants';
import { Button } from '../../common/Button/Button';

export const Header = () => {
	return (
		<header className='d-flex justify-content-between align-items-center flex-wrap'>
			<Logo />
			<div className='d-flex gap-4'>
				<div className='username'>{USERNAME}</div>
				<Button buttonText={LOGOUT} />
			</div>
		</header>
	);
};
