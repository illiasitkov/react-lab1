import React, { useState } from 'react';
import { Input } from '../../common/Input/Input';
import { Button } from '../../common/Button/Button';
import { Textarea } from '../../common/Textarea/Textarea';

import './CreateCourse.css';
import { Author } from './components/Author/Author';
import { changeHandler } from '../../helpers/changeHandler';
import { AddAuthor } from './components/AddAuthor/AddAuthor';
import { Duration } from './components/Duration/Duration';
import { AuthorList } from './components/ShowAuthors/ShowAuthors';
import {
	AUTHORS,
	COURSE_AUTHORS,
	DESCRIPTION,
	DESCRIPTION_PLACEHOLDER,
	FILL_FIELDS_ALERT,
	TITLE,
	TITLE_PLACEHOLDER,
} from '../../constants';

export const CreateCourse = ({ authors, addNewAuthor, addNewCourse }) => {
	const [authorIds, setAuthorIds] = useState([]);
	const [minutes, setMinutes] = useState(0);
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');

	const correctInputs = () => {
		return (
			title && description.length > 1 && minutes > 0 && authorIds.length > 0
		);
	};

	const clearInputs = () => {
		setTitle('');
		setDescription('');
		setAuthorIds([]);
		setMinutes(0);
	};

	const addCourse = () => {
		if (correctInputs()) {
			addNewCourse(title, description, authorIds, minutes);
			clearInputs();
		} else {
			alert(FILL_FIELDS_ALERT);
		}
	};

	const addAuthorToList = (authorId) => {
		setAuthorIds(authorIds.concat(authorId));
	};

	const deleteAuthorFromList = (authorId) => {
		setAuthorIds(authorIds.filter((id) => id !== authorId));
	};

	const authorsToAdd = authors
		.filter((a) => !authorIds.includes(a.id))
		.map((a) => {
			return (
				<div key={a.id} className='mb-2'>
					<Author author={a} onAdd={() => addAuthorToList(a.id)} />
				</div>
			);
		});

	const authorsToDelete = authorIds
		.map((id) => authors.filter((author) => author.id === id)[0])
		.map((a) => {
			return (
				<div key={a.id} className='mb-2'>
					<Author
						deleteMode={true}
						author={a}
						onDelete={() => deleteAuthorFromList(a.id)}
					/>
				</div>
			);
		});

	return (
		<section className='content-wrapper'>
			<div className='mb-4 d-flex align-items-end justify-content-between'>
				<Input
					width='350px'
					type='text'
					value={title}
					onChange={changeHandler(setTitle)}
					id='courseTitle'
					labelText={TITLE}
					inputPlaceholder={TITLE_PLACEHOLDER}
				/>
				<Button onClick={addCourse} buttonText='Create course' />
			</div>
			<div className='mb-4'>
				<Textarea
					id='courseDescription'
					value={description}
					onChange={changeHandler(setDescription)}
					labelText={DESCRIPTION}
					placeholderText={DESCRIPTION_PLACEHOLDER}
				/>
			</div>
			<div className='black-border'>
				<div className='row'>
					<div className='col-6 gap-vertical-lg'>
						<AddAuthor addNewAuthor={addNewAuthor} />
						<Duration setMinutes={setMinutes} minutesValue={minutes} />
					</div>
					<div className='col-6 author-list gap-vertical-md'>
						<AuthorList title={AUTHORS} authorViewList={authorsToAdd} />
						<AuthorList
							title={COURSE_AUTHORS}
							authorViewList={authorsToDelete}
						/>
					</div>
				</div>
			</div>
		</section>
	);
};

// import React, { useState } from 'react';
// import { Input } from '../../common/Input/Input';
// import { Button } from '../../common/Button/Button';
// import { Textarea } from '../../common/Textarea/Textarea';
//
// import './CreateCourse.css';
// import { Author } from './components/Author/Author';
// import { durationPipeToString } from '../../helpers/durationPipe';
//
// export const CreateCourse = ({ authors, addNewAuthor, addNewCourse }) => {
// 	const [authorIds, setAuthorIds] = useState([]);
// 	const [minutes, setMinutes] = useState(0);
// 	const [title, setTitle] = useState('');
// 	const [description, setDescription] = useState('');
//
// 	const [authorName, setAuthorName] = useState('');
//
// 	const changeHandler = (setter) => (e) => {
// 		setter(e.target.value);
// 	};
//
// 	const correctInputs = () => {
// 		return (
// 			title && description.length > 1 && minutes > 0 && authorIds.length > 0
// 		);
// 	};
//
// 	const addAuthor = () => {
// 		if (authorName.length > 1) {
// 			addNewAuthor(authorName);
// 			setAuthorName('');
// 		} else {
// 			alert('Author name is too short!');
// 		}
// 	};
//
// 	const clearInputs = () => {
// 		setTitle('');
// 		setDescription('');
// 		setAuthorIds([]);
// 		setMinutes(0);
// 	};
//
// 	const addCourse = () => {
// 		if (correctInputs()) {
// 			addNewCourse(title, description, authorIds, minutes);
// 			clearInputs();
// 		} else {
// 			alert('Please, fill in all the fields');
// 		}
// 	};
//
// 	const addAuthorToList = (authorId) => {
// 		setAuthorIds(authorIds.concat(authorId));
// 	};
//
// 	const deleteAuthorFromList = (authorId) => {
// 		setAuthorIds(authorIds.filter((id) => id !== authorId));
// 	};
//
// 	const authorsToAdd = authors
// 		.filter((a) => !authorIds.includes(a.id))
// 		.map((a) => {
// 			return (
// 				<div key={a.id} className='mb-2'>
// 					<Author author={a} onAdd={() => addAuthorToList(a.id)} />
// 				</div>
// 			);
// 		});
//
// 	const authorsToDelete = authorIds
// 		.map((id) => authors.filter((author) => author.id === id)[0])
// 		.map((a) => {
// 			return (
// 				<div key={a.id} className='mb-2'>
// 					<Author
// 						deleteMode={true}
// 						author={a}
// 						onDelete={() => deleteAuthorFromList(a.id)}
// 					/>
// 				</div>
// 			);
// 		});
//
// 	return (
// 		<section className='courses-list'>
// 			<div className='container'>
// 				<div className='row mb-4'>
// 					<div className='col-12 d-flex align-items-end justify-content-between'>
// 						<Input
// 							width='350px'
// 							type='text'
// 							value={title}
// 							onChange={changeHandler(setTitle)}
// 							id='courseTitle'
// 							labelText='Title'
// 							inputPlaceholder='Enter title...'
// 						/>
// 						<Button onClick={addCourse} buttonText='Create course' />
// 					</div>
// 				</div>
// 				<div className='row mb-3'>
// 					<div className='col-12'>
// 						<Textarea
// 							id='courseDescription'
// 							value={description}
// 							onChange={changeHandler(setDescription)}
// 							labelText='Description'
// 							placeholderText='Enter description'
// 						/>
// 					</div>
// 				</div>
// 				<div className='row'>
// 					<div className='col-12'>
// 						<div className='container black-border'>
// 							<div className='row'>
// 								<div className='col-6'>
// 									<div className='mb-5'>
// 										<h4 className='text-center mb-4'>Add author</h4>
// 										<Input
// 											inputPlaceholder='Enter author name...'
// 											type='text'
// 											value={authorName}
// 											onChange={changeHandler(setAuthorName)}
// 											id='authorName'
// 											width='100%'
// 											labelText='Author name'
// 										/>
// 										<div className='mt-4 d-flex justify-content-center'>
// 											<Button buttonText='Create author' onClick={addAuthor} />
// 										</div>
// 									</div>
// 									<div className='mb-3'>
// 										<h4 className='text-center mb-4'>Duration</h4>
// 										<Input
// 											inputPlaceholder='Enter duration in minutes...'
// 											type='number'
// 											value={minutes}
// 											onChange={changeHandler(setMinutes)}
// 											id='authorName'
// 											width='100%'
// 											labelText='Duration'
// 										/>
// 									</div>
// 									<div>
// 										<span className='duration-label'>
// 											Duration:&nbsp;
// 											<span className='duration-time'>
// 												{durationPipeToString(minutes)}
// 											</span>
// 											&nbsp;hours
// 										</span>
// 									</div>
// 								</div>
// 								<div className='col-6'>
// 									<div className='mb-5'>
// 										<h4 className='text-center mb-4'>Authors</h4>
// 										{authorsToAdd}
// 										{authorsToAdd.length <= 0 && (
// 											<p className='text-center'>Author list is empty</p>
// 										)}
// 									</div>
// 									<div>
// 										<h4 className='text-center mb-4'>Course authors</h4>
// 										{authorsToDelete}
// 										{authorsToDelete.length <= 0 && (
// 											<p className='text-center'>Author list is empty</p>
// 										)}
// 									</div>
// 								</div>
// 							</div>
// 						</div>
// 					</div>
// 				</div>
// 			</div>
// 		</section>
// 	);
// };
