import React from 'react';
import { Input } from '../../../../common/Input/Input';
import { changeHandler } from '../../../../helpers/changeHandler';
import { durationPipeToString } from '../../../../helpers/durationPipe';

export const Duration = ({ minutesValue, setMinutes }) => {
	return (
		<div>
			<h4 className='text-center mb-3'>Duration</h4>
			<Input
				inputPlaceholder='Enter duration in minutes...'
				type='number'
				value={minutesValue}
				onChange={changeHandler(setMinutes)}
				id='authorName'
				width='100%'
				labelText='Duration'
			/>
			<div className='mt-2'>
				<span className='duration-label'>
					Duration:&nbsp;
					<span className='duration-time'>
						{durationPipeToString(minutesValue)}
					</span>
					&nbsp;hours
				</span>
			</div>
		</div>
	);
};
