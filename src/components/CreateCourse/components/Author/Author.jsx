import React from 'react';
import { Button } from '../../../../common/Button/Button';
import { ADD_AUTHOR, DELETE_AUTHOR } from '../../../../constants';

export const Author = ({ author, onDelete, onAdd, deleteMode = false }) => {
	return (
		<div className='d-flex flex-wrap justify-content-between align-items-center'>
			<span>{author.name}</span>
			<Button
				onClick={deleteMode ? onDelete : onAdd}
				buttonText={deleteMode ? DELETE_AUTHOR : ADD_AUTHOR}
			/>
		</div>
	);
};
