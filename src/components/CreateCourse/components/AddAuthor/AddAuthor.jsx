import React, { useState } from 'react';
import { Input } from '../../../../common/Input/Input';
import { Button } from '../../../../common/Button/Button';
import { changeHandler } from '../../../../helpers/changeHandler';

export const AddAuthor = ({ addNewAuthor }) => {
	const [authorName, setAuthorName] = useState('');

	const addAuthor = () => {
		if (authorName.length > 1) {
			addNewAuthor(authorName);
			setAuthorName('');
		} else {
			alert('Author name is too short!');
		}
	};

	return (
		<div className='add-author'>
			<h4 className='text-center mb-3'>Add author</h4>
			<Input
				inputPlaceholder='Enter author name...'
				type='text'
				value={authorName}
				onChange={changeHandler(setAuthorName)}
				id='authorName'
				width='100%'
				labelText='Author name'
			/>
			<div className='mt-3 d-flex justify-content-center'>
				<Button buttonText='Create author' onClick={addAuthor} />
			</div>
		</div>
	);
};
