import React from 'react';

export const AuthorList = ({ authorViewList, title }) => {
	return (
		<div>
			<h4 className='text-center mb-4'>{title}</h4>
			{authorViewList}
			{authorViewList.length <= 0 && (
				<p className='text-center'>Author list is empty</p>
			)}
		</div>
	);
};
