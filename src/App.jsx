import React, { useState } from 'react';
import { Header } from './components/Header/Header';
import { Courses } from './components/Courses/Courses';
import { Routes, Route, BrowserRouter, Navigate } from 'react-router-dom';
import { v4 } from 'uuid';

import { mockedCoursesList } from './data/mockedCourses';
import { mockedAuthorsList } from './data/mockedAuthors';

import './App.css';
import { CreateCourse } from './components/CreateCourse/CreateCourse';
import { getCurrentDateFormatted } from './helpers/datePipe';

function App() {
	const [courses, setCourses] = useState(mockedCoursesList);
	const [authors, setAuthors] = useState(mockedAuthorsList);

	const addNewAuthor = (name) => {
		setAuthors(
			authors.concat({
				name,
				id: v4(),
			})
		);
	};

	const addNewCourse = (title, description, authors, duration) => {
		setCourses(
			courses.concat({
				title,
				id: v4(),
				description,
				authors,
				duration,
				creationDate: getCurrentDateFormatted(),
			})
		);
	};

	return (
		<div>
			<BrowserRouter>
				<Header />
				<Routes>
					<Route
						exact
						path='/courses'
						element={<Courses courses={courses} authors={authors} />}
					/>
					<Route
						exact
						path='courses/new'
						element={
							<CreateCourse
								addNewCourse={addNewCourse}
								addNewAuthor={addNewAuthor}
								authors={authors}
							/>
						}
					/>
					<Route path='*' element={<Navigate to='/courses' />} />
				</Routes>
			</BrowserRouter>
		</div>
	);
}

export default App;
